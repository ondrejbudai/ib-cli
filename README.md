# ib-cli

A simple wrapper around [weldr-client](https://github.com/osbuild/weldr-client/) that can build an image in one command.

## Installation

Let's keep it simple for now:

```
git clone https://gitlab.com/ondrejbudai/ib-cli.git
ln -rs ib-cli/src/main.py ~/.local/bin/ib-cli
```


## Usage

The following command will build a qcow2 image from the `base.toml` blueprint. When the image is built, it will
be saved to the `base.qcow2` file.

```
ib-cli base.toml qcow2 base.qcow2
```
