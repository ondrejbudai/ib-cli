#!/usr/bin/python3
import argparse
import json
import os.path
import subprocess
import sys
import time
import tomllib


def wait_for_compose(compose_id: str) -> str:
    while True:
        result = subprocess.run(
            ["composer-cli", "--json", "compose", "info", compose_id],
            check=True,
            capture_output=True,
            encoding="utf-8",
        )

        compose_data = json.loads(result.stdout)

        status = compose_data[0]["body"]["queue_status"]
        if status not in ["RUNNING", "WAITING"]:
            return status

        time.sleep(10)


def main() -> int:
    parser = argparse.ArgumentParser(
        description="Wrapper around composer-cli to build images with just one command."
    )

    parser.add_argument("blueprint", help="Path to the blueprint file")
    parser.add_argument("image_type", help="Image type to build")
    parser.add_argument("output", help="Path where the output will be put")
    args = parser.parse_args()

    if os.path.exists(args.output):
        print(f"The output file ({args.output}) already exists! Exiting...")
        return 1

    with open(args.blueprint, mode="rb") as f:
        bp = tomllib.load(f)
        bp_name = bp["name"]

    subprocess.run(["composer-cli", "blueprints", "push", args.blueprint], check=True)
    print("Blueprint uploaded")

    result = subprocess.run(
        [
            "composer-cli",
            "--json",
            "compose",
            "start",
            bp_name,
            args.image_type,
        ],
        check=True,
        capture_output=True,
        encoding="utf-8",
    )
    compose_data = json.loads(result.stdout)
    compose_id = compose_data[0]["body"]["build_id"]

    print(f"Compose {compose_id} started")

    try:
        result = wait_for_compose(compose_id)
    except Exception:
        subprocess.run(["composer-cli", "compose", "cancel", compose_id])
        subprocess.run(["composer-cli", "compose", "delete", compose_id])
        raise

    if result != "FINISHED":
        print(f"Composed {compose_id} finished in invalid state: {result}")
        return 1

    subprocess.run(
        ["composer-cli", "compose", "image", compose_id, "--filename", args.output]
    )

    subprocess.run(["composer-cli", "compose", "delete", compose_id])

    return 0


if __name__ == "__main__":
    sys.exit(main())
